#!/usr/bin/env python
import os
import tornado
import traceback

from Libs import Logger
from tornado import web

def main():
    logger = Logger.get_logger()
    try:
        io_loop = tornado.ioloop.IOLoop.instance()
        pool_handler = App.PoolHandler(logger=logger)
        ufo_cloud = App.WebSocketClient(ws_url="ws://localhost:8889/ws",
                                        pass_ws_token=True,
                                        token_url="http://localhost:8889",
                                        try_reconnect=True)
        env = Helpers.Config.load_environment()

        # main API controller
        api = Api(pool_handler=pool_handler,
                  db_operations=db_operations, logger=logger)
        ufo_decoder.set_params(pool_handler=pool_handler,
                               api=api,
                               client_type='ufo_decoder',
                               ufo_id=ufo_id,
                               ufo_hash=ufo_hash
                               )
        ufo_decoder.connect()
        ufo_decoder.add_to_ioloop(io_loop)
        ufo_cloud.set_params(pool_handler=pool_handler,
                             api=api,
                             client_type='ufo_cloud',
                             ufo_id=ufo_id,
                             ufo_hash=ufo_hash
                             )
        # ufo_cloud.connect()
        app = tornado.web.Application([
            (r"/", App.RequestHandler,
             {'pool_handler': pool_handler, 'api': api}),
            (r"/ws/(.*)", App.WebSocketHandler, {
                'pool_handler': pool_handler,
                'api': api
            })],
            static_path=os.path.join(os.path.dirname(
                __file__), "..", "..", "..", "static")
        )

        # Adding Job for Bootstrap (Server Initialization)
        job = scheduler.add_job(App.Helpers.Scheduler.actuation, 'date',
                                args=[{'is_bootstrap': True}],
                                run_date=time.strftime(
                                    '%Y-%m-%d %H:%M:%S',
                                    time.localtime(time.time() + 1)
                                ),
                                timezone=timezone('Asia/Kolkata')
                                )

        app.listen(8080)
        print('Server started.')
        print('listening on 8080 ...')

        io_loop.start()
    except Exception as e:
        print(e.message)

        print(traceback.format_exc())
        logger.error(e.message)


if __name__ == '__main__':
    main()

    # server = tornado.httpserver.HTTPServer(ws_app)
    # server = tornado.httpserver.HTTPServer(ws_app, ssl_options={
    #   "certfile": "../ssl_certs/cloud.phynart.com.cert",
    #   "keyfile": "../ssl_certs/cloud.phynart.com.key",
    # })
    # server.listen(8888)
    # tornado.ioloop.IOLoop.instance().start()
