from Helpers import Config
import logging
import logging.handlers
from pythonjsonlogger import jsonlogger
import os
import errno
import sys

class Logger():
	def get_logger(self):
		try:
			env = Config.load_environment()
			os.makedirs(env['log_path'])
		except OSError, e:
			if e.errno != errno.EEXIST:
				raise
		DEBUG_LEVEL_TRAFFIC = 21
		logging.addLevelName(DEBUG_LEVEL_TRAFFIC, "TRAFFIC")
		def traffic(self, message, *args, **kws):
			# Yes, logger takes its '*args' as 'args'.
			if self.isEnabledFor(DEBUG_LEVEL_TRAFFIC):
				self._log(DEBUG_LEVEL_TRAFFIC, message, args, **kws) 

		logging.Logger.traffic = traffic
 
		logger = logging.getLogger()

		numeric_level = getattr(logging, env['log_level'].upper(), None)

		if not isinstance(numeric_level, int):
			raise ValueError('Invalid log level: %s' % env['log_level'])
		logger.setLevel(level=numeric_level)

		formatter = jsonlogger.JsonFormatter('%(asctime)s %(levelname)s %(name)s %(message)s')
		log_handler = logging.handlers.TimedRotatingFileHandler(
			filename=os.path.join(env['log_path'], env['log_file']),
			when='D',
			interval=1,
			backupCount=env['log_backup_count']
		)
		log_handler.setFormatter(formatter)
		logger.addHandler(log_handler)
		if env['log_level'] == 'DEBUG':
			stdout = logging.StreamHandler(sys.stdout)
			logger.addHandler(stdout)
		return logger