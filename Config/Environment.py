import os
settings = {
	'process_name': 'ufo_server',
	#database config
	'db_config_name': 'sqlite',
	'mysql': {
		'db_host': 'localhost',
		'db_port': '3306',
		'db_name': 'ufo',
		'db_user': 'root',
		'db_pass': ''
	},
	'sqlite': {
		'db_path': 'C:\logs' if os.name == 'nt' else '/tmp/logs',
		'db_file': 'ufo_server.db'
	},
	'scheduler': {
		'db_path': 'C:\logs' if os.name == 'nt' else '/tmp/logs',
		'db_file': 'runtime_schedules.db'
	},
	#logging
	'log_level' : 'DEBUG',
	#'log_path': '/var/log',
	'log_path': 'C:\logs' if os.name == 'nt' else '/tmp/logs',
	'log_file': 'ufo_server.log',
	'log_rotate_size': 1024000,
	'log_backup_count': 30
}