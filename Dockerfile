## YashOne
# Using RHEL 7 base image

# Pull the rhel image from the local repository
FROM registry.access.redhat.com/rhel7:7.4
MAINTAINER connect-tech@redhat.com

### Required Atomic/OpenShift Labels - https://github.com/projectatomic/ContainerApplicationGenericLabels#####
LABEL name="YashOne" \
      vendor="Yash Solutions LLC" \
      version="2.0" \
      release="13" \
      run='docker run -d -p 80:80 --name=YashOne yashone:preview2' \
      summary="AI backed, event-trigger based platform." \
      description="Integrates all your tools and helps build feedback loops for automation with great visualization."


COPY ./EULA.txt /licenses/
COPY ./ /app/

#COPY ./rhel-internal.repo /etc/yum.repos.d/
RUN yum repolist
### Add your package needs here
RUN yum localinstall -y --nogpgcheck http://dl.fedoraproject.org/pub/epel/7/x86_64/Packages/e/epel-release-7-11.noarch.rpm
#RUN yum remove -y glibc-common
#RUN yum -y install python-setuptools
RUN yum -y install python36 python34-pip
RUN yum clean all
#RUN rm -f /etc/yum.repos.d/rhel-internal.repo
# Install the application
RUN pip3.4 install virtualenv tornado
EXPOSE 80

#Start the service
WORKDIR /app
ENTRYPOINT ["/usr/bin/python3", "/app/server.py"]
