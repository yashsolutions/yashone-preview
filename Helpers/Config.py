import importlib
import Config
import os

def load_environment():
	env = os.environ.get('KEY_THAT_MIGHT_EXIST')
	if env:
		config = importlib.import_module('Config.Environment', env)
		return getattr(config, 'settings')
	else:
		config = importlib.import_module('Config.Environment', 'Dev')
		print config
		return getattr(config, 'settings')