import tornado
import tornado.ioloop
import tornado.web
import tornado.websocket

client = 'YashOne'


class MainHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        snap = self.get_argument('snap', None)
        self.render('page-elements/index.html', show='dash', client=client, snap=snap)

    def post(self):
        self.render('page-elements/index.html', show='dash', client=client)


class DashHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        snap = self.get_argument('snap', None)
        self.render('page-elements/index.html', show='dash', client=client, snap=snap)

class DashLoBHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        snap = self.get_argument('snap', None)
        self.render('page-elements/index.html', show='dashlineofbusiness', client=client, snap=snap)

class DashITHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        snap = self.get_argument('snap', None)
        self.render('page-elements/index.html', show='dashitstatus', client=client, snap=snap)

class DashSDelHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        snap = self.get_argument('snap', None)
        self.render('page-elements/index.html', show='dashsoftdelivery', client=client, snap=snap)

class DashSQHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        snap = self.get_argument('snap', None)
        self.render('page-elements/index.html', show='dashsoftquality', client=client, snap=snap)

class DashSDevHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        snap = self.get_argument('snap', None)
        self.render('page-elements/index.html', show='dashsoftdevelopment', client=client, snap=snap)

class AppsHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        self.render('page-elements/index.html', show='apps', client=client)


class DeploymentsHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        self.render('page-elements/index.html', show='deployments', client=client)


class BugsHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        self.render('page-elements/index.html', show='bugs', client=client)


class AppsTodoHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        tab = self.get_argument('tab', 'timeline')
        self.render('page-elements/index.html', show='apps-todo', tab=tab, client=client)


class EnvsHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        self.render('page-elements/index.html', show='envs', client=client)


class EnvsTodoAppProdHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        tab = self.get_argument('tab', 'timeline')
        self.render('page-elements/index.html', show='envs-todo-app-prod', tab=tab, client=client)


class EnvsTodoAppStageHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        tab = self.get_argument('tab', 'timeline')
        self.render('page-elements/index.html', show='envs-todo-app-stage', tab=tab, client=client)


class DatabasesHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        self.render('page-elements/index.html', show='databases', client=client)


class DatabasesDataDbHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        self.render('page-elements/index.html', show='databases-data-db', client=client)


class ProjectsHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        self.render('page-elements/index.html', show='projects', client=client)


class ProjectsTodoHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        tab = self.get_argument('tab', 'timeline')
        self.render('page-elements/index.html', show='projects-todo', tab=tab, client=client)


class MonitoringHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        self.render('page-elements/index.html', show='monitoring', client=client)


class JobQueueHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        self.render('page-elements/index.html', show='job-queue', client=client)


class JobQueueLogsHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        self.render('page-elements/index.html', show='job-queue-logs', client=client)


class PluginJenkinsHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        self.render('page-elements/index.html', show='plugin-jenkins', client=client)


class ProfileHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        tab = self.get_argument('tab', 'activity')
        self.render('page-elements/index.html', show='profile', tab=tab, client=client)


class LoginHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        self.render('page-elements/login.html', client=client)


class LockHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        self.render('page-elements/lock.html', client=client)


class AnalyticsHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        snap = self.get_argument('snap', None)
        self.render('page-elements/index.html', show='analytics', client=client, snap=snap)


class InfraHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        self.render('page-elements/index.html', show='infra', client=client)


class InfraPlatformHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        self.render('page-elements/index.html', show='infra-platform', client=client)


class InfraCloudLocationsHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        self.render('page-elements/index.html', show='infra-cloud-locations', client=client)


class InfraDelphixHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        self.render('page-elements/index.html', show='infra-delphix', client=client)


class InfraDelphixEngine9Handler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        self.render('page-elements/index.html', show='infra-delphix-9', client=client)

class InfraActifioHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        self.render('page-elements/index.html', show='infra-actifio', client=client)


class InfraActifioAppliance9Handler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        self.render('page-elements/index.html', show='infra-actifio-9', client=client)

class UsersHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        self.render('page-elements/index.html', show='users', client=client)


class UsersListHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        self.render('page-elements/index.html', show='users-list', client=client)


class SettingsHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        tab = self.get_argument('tab', 'timeline')
        self.render('page-elements/index.html', show='settings', tab=tab, client=client)


class SamplePageHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        self.render('page-elements/index.html', show='sample-page', client=client)


class My404Handler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    # Override prepare() instead of get() to cover all possible HTTP methods.
    def prepare(self):
        self.set_status(404)
        self.render("page-elements/index.html", show='error404', client=client)


class EchoWebSocket(tornado.websocket.WebSocketHandler):
    def data_received(self, chunk):
        pass

    def open(self):
        print("WebSocket opened")

    def on_message(self, message):
        for count in range(10):
            self.write_message(u"You said: " + message + " but I said it " + str(count) + " times.")

    def on_close(self):
        print("WebSocket closed")


urlSpec = [
        (r"/", MainHandler),
        (r"/dash", DashHandler),
        (r"/dashlineofbusiness", DashLoBHandler),
        (r"/dashitstatus", DashITHandler),
        (r"/dashsoftdelivery", DashSDelHandler),
        (r"/dashsoftquality", DashSQHandler),
        (r"/dashsoftdevelopment", DashSDevHandler),
        (r"/apps", AppsHandler),
        (r"/deployments", DeploymentsHandler),
        (r"/bugs", BugsHandler),
        (r"/apps/todo-app", AppsTodoHandler),
        (r"/environments", EnvsHandler),
        (r"/environments/todo-app/prod", EnvsTodoAppProdHandler),
        (r"/environments/todo-app/stage", EnvsTodoAppStageHandler),
        (r"/databases", DatabasesHandler),
        (r"/databases/dataDb", DatabasesDataDbHandler),
        (r"/projects", ProjectsHandler),
        (r"/projects/todo", ProjectsTodoHandler),
        (r"/monitoring", MonitoringHandler),
        (r"/job-queue", JobQueueHandler),
        (r"/job-queue/logs", JobQueueLogsHandler),
        (r"/profile", ProfileHandler),
        (r"/login", LoginHandler),
        (r"/lock", LockHandler),
        (r"/users", UsersHandler),
        (r"/users/list", UsersListHandler),
        (r"/analytics", AnalyticsHandler),
        (r"/infra", InfraHandler),
        (r"/infra/platform", InfraPlatformHandler),
        (r"/infra/cloud-locations", InfraCloudLocationsHandler),
        (r"/infra/delphix", InfraDelphixHandler),
        (r"/infra/delphix/engine9", InfraDelphixEngine9Handler),
        (r"/infra/actifio", InfraActifioHandler),
        (r"/infra/actifio/appliance9", InfraActifioAppliance9Handler),
        (r"/settings", SettingsHandler),
        (r"/pages/sample", SamplePageHandler),
        (r"/plugin/jenkins", PluginJenkinsHandler),
        (r'/(favicon\.ico)', tornado.web.StaticFileHandler, {'path': '/'}),
        (r'/bower_components/(.*)', tornado.web.StaticFileHandler, {'path': 'bower_components'}),
        (r'/dist/(.*)', tornado.web.StaticFileHandler, {'path': 'dist'}),
        (r'/plugins/(.*)', tornado.web.StaticFileHandler, {'path': 'plugins'}),
        (r'/webSocket', EchoWebSocket)]


def make_app():
    return tornado.web.Application(urlSpec, default_handler_class=My404Handler)


if __name__ == "__main__":
    app = make_app()
    app.settings['debug'] = True
    app.settings['autoreload'] = True
    app.settings['compiled_template_cache'] = False
    app.listen(80)
    print('YashOne started on port 80')
    tornado.ioloop.IOLoop.current().start()
